var User = require("./models/user");

var middlewareObj = {};

middlewareObj.isLoggedIn = function isLoggedIn(request, response, next) {
    if (request.isAuthenticated()) {
        return next();
    }

    request.flash("Error", "You must be logged in to do that");
    response.redirect("/login");
};

module.exports = middlewareObj;