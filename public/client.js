var items;
var currentItem = 0;


$(document).on("mouseenter", "img", function() {
     //console.log("HOVER");
   //$('.description').show(); 
});

$(document).on("mouseleave", "img", function() {
    //$('.description').hide();
})

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        console.log("Reached bottom");
           addrow();
            addrow();
        addrow();
    }
});


function addrow() {
    var temp = currentItem;
    
    var rowStart = "<div class='row'>";
    var rowEnd = "</div>";
    
    var fullString = rowStart;
    
    for (var i = temp; i < (temp + 3); i++) {
        if (i < items.length) {
            fullString = fullString + getImageHtml(i);
            currentItem++;
        }
    }
    
    fullString = fullString + rowEnd;
    
    $(fullString).appendTo("#photocontainer");
}

function getImageHtml(itemNum) {
    var item = items[itemNum];
            
    var thumbnail = "thumbs\\" +  item.FileName;
    var fullFile = "images\\" + item.FileName;

    var caption = "<div class='caption'><h3>" + item.Title + "</h3><p class='txt-sm'>" + item.Description + "</p></div>";
    var html = "<div class='col-lg-4'><div class='thumbnail'><a href='" +
     fullFile + "' data-lightbox='" + "image" + itemNum 
     + "' ><img class='img-resegergeponsive' src='" + thumbnail 
     + "'></a>" + caption + "</div></div>";
    
    return html;
    
}

function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}