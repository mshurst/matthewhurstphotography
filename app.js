var express = require("express");
var app = express();
var bodyparser = require("body-parser");
var AWS = require("aws-sdk");
var methodOverride = require("method-override");
var uuid = require("uuid/v1");
var mongoose = require("mongoose");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var flash = require("connect-flash");
var User = require("./models/user");
var middleware = require("./middleware");

var indexRoutes = require("./routes/index");

var allData = {};

AWS.config.update({
    accessKeyId : process.env.AWS_ACCESS_KEY,
    secretAccessKey : process.env.AWS_SECRET_KEY,
    region : "eu-west-1"
});



mongoose.connect("mongodb://admin:"+process.env.DB_PASSWORD+"@ds245680.mlab.com:45680/mshurst-admin");

app.use(require("express-session")({
    secret : "this is the secret",
    resave : false,
    saveUninitialized : false
}));


app.use(bodyparser.urlencoded({extended:true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(request, response, next) {
    response.locals.currentUser = request.user;
    response.locals.error = request.flash("error");
    response.locals.success = request.flash("success");
    next();
});

var s3 = new AWS.S3();
var dynamoManager = new AWS.DynamoDB();
var dynamodb = new AWS.DynamoDB.DocumentClient();
var dynamoTable = "PhotographyMetaData";

var mainBucket = "mshurst-photography-input";
var thumbBucket = "mshurst-photography-thumbs";

//copyImagesFromS3();

updateImages();

function copyImagesFromS3() {
    dynamoManager.deleteTable({
        TableName : dynamoTable
    }, function(err, data) {
        //if (err) {
            console.log("Could not delete table. " + err);
            //return;
        //} else {
            var createParams = {
                TableName : dynamoTable,
                KeySchema : [
                    {AttributeName : "id", KeyType : "HASH"}
                ],
                AttributeDefinitions: [
                    {AttributeName : "id", AttributeType : "S"}
                ],
                ProvisionedThroughput: {
                    ReadCapacityUnits : 5,
                    WriteCapacityUnits : 5
                }
            };

            dynamoManager.createTable(createParams, function(err, data) {
                if (err) {
                    console.log("Could not create new table : " + err);;
                    return;
                } else {
                    console.log("Table created");
                }      
            });

            setTimeout(function() {
                s3.listObjectsV2({
                    Bucket : mainBucket
                }, function(err, data) {
                    if (err) {
                        console.log("Could not get data from s3");
                    } else {
                        console.log(data);
            
                        data.Contents.forEach(function(item) {
                            var params = {
                                TableName : dynamoTable,
                                Item : {
                                    "id" : uuid(),
                                    "image" : item.Key,
                                    "title" : item.Key,
                                    "description" : "This is the description"
                                }
                            };
            
                            dynamodb.put(params, function(err, addedItem) {
                                if (err) {
                                    console.log("Error creating item. " + err);
                                } else {
                                    console.log("Item added");
                                }
                            })
                        })
                    }
                })
            }, 10000);
        //}
    })    
}

function updateImages() {

    var params = {
        TableName : dynamoTable
    };

    dynamodb.scan(params, function(err, data) {
        if (err) {
            console.log("Error getting data from Dynamo. " + err);
        } else {
            //console.log(data);
            allData = data;
            //console.log(allData);
        }
    })
}

app.use(indexRoutes);

app.get("/", function(request, response) {
    //response.render("index", {images : shuffle(allData.Items)});
    response.render("index", {images : allData.Items.sort(orderByDescription)});
});

app.get("/about", function(request, response) {
    response.render("about");
});

app.listen(3000, function() {
    console.log("Currently listening");
});

app.post("/upload", middleware.isLoggedIn, function(request, response) {
    if (request.files.file !== undefined) {
        response.redirect("/uploads");
    } else {
        response.send("Error. No file chosen");
    }
})

app.get("/sign-s3", function(request, response) {
    const fileName = request.query["file-name"];
    const fileType = request.query["file-type"];
    const s3Params = {
        Bucket : mainBucket,
        Key : fileName,
        Expires : 60,
        ContentType : fileType,
        ACL : 'public-read'
    };

    s3.getSignedUrl("putObject", s3Params, function(error, data) {
        if (error) {
            console.log(error);
            return response.end();
        }

        const returnData = {
            signedRequest : data,
            url : "https://" + mainBucket + ".s3.amazonaws.com/" + fileName
        };
        response.write(JSON.stringify(returnData));
        response.end();
    });
});

//CREATE
app.post("/create", middleware.isLoggedIn, function(request, response) {
    //data is already in s3, put it in dynamo too then update images

    var params = {
        TableName : dynamoTable,
        Item : {
            "id" : uuid(),
            "title" : request.params.title,
            "image" : request.params.image,
            "description" : request.params.description
        }
    };

    dynamodb.put(params, function(err, data) {
        if (err) {
            console.log("ERROR creating item : " + err);
            response.redirect("back");
        } else {
            console.log("Added item");
        }
    })

    updateImages();
    response.redirect("/");
});

//update
app.put("/:id", middleware.isLoggedIn, function(request, response) {
    //get the item from dynamoDB table using request.body.Key

    //response.send("YOU HIT THE UPDATE ENDPOINT FOR ID : " + request.params.id);

    var params = {
        TableName : dynamoTable,
        Key : {
            "id" : request.params.id
        },
        UpdateExpression : "set description = :d, title = :t, orientation = :o",
        ExpressionAttributeValues: {
            ":d":request.body.description,
            ":t":request.body.title,
            ":o":request.body.optorientation
        },
        ReturnValues : "UPDATED_NEW"
    };

    dynamodb.update(params, function(err, data) {
        if (err) {
            console.log("Error udpating. " + JSON.stringify(err, null, 2));
            response.redirect("/");
        } else {
            console.log("Updated item : " + JSON.stringify(data, null, 2));
            updateImages();

            setTimeout(function() {
                response.redirect("/");
            }, 1500);            
        }
    })    
});

//edit
app.get("/edit/:id", middleware.isLoggedIn, function(request, response) {
    //get the item from dynamoDB
    console.log("Request id : " + request.params.id);
    var params = {
        TableName : dynamoTable,
        Key : {
            "id" : request.params.id
        }
    };

    dynamodb.get(params, function(err, data) {
        if (err) {
            console.error("Unable to read item. " + JSON.stringify(err, null, 2));
            //response.send("Could not get item");
            response.redirect("/");
        } else {
            console.log("Got item : " + JSON.stringify(data, null, 2));
            //response.send("GOT ITEM: " +  JSON.stringify(data, null, 2));
            //updateImages();
            response.render("edit", {item : data.Item});
        }
    });    
});


//delete
app.delete("/:id", middleware.isLoggedIn, function(request, response) {
    //get the item from dynamo & delete

    console.log("Item to delete : " + request.params.id);

    var params = {
        TableName : dynamoTable,
        Key : {
            "id" : request.params.id
        }
    };

    dynamodb.delete(params, function(err, data) {
        if (err) {
            console.log("Error deleting item : " + err);
            response.redirect("back");
        } else {
            //console.log("Delete item : " + JSON.stringify(data, null, 2));

            //console.log(allData);
            allData.Items.forEach(function(item) {
                if (item.id ===  request.params.id) {
                    console.log("Found id at " + allData.Items.indexOf(item));

                    allData.Items.splice(allData.Items.indexOf(item), 1);
                }
            });            

            response.redirect("/");
        }
    })
});

app.get("/upload", middleware.isLoggedIn, function(request, response) {
    response.render("new");
});

function shuffle(array) {
    //console.log(array);
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function orderByTitle(a, b) {
    if (a.title < b.title) {
        return -1;
    }
    if (a.title > b.title) {
        return 1;
    }
    return 0;
}

function orderByDescription(a,b) {
    if (a.description < b.description) {
        return -1;
    }
    if (a.description > b.description) {
        return 1;
    }
    return 0;
}

function orderByOrientation(a, b) {
    if (a.orientation < b.orientation)
        return -1;
    if (a.orientation > b.orientation)
        return 1;
    return 0;
}


