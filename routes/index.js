var express = require("express");
var router = express.Router();
var passport = require("passport");
var User = require("../models/user.js");

//auth routes

//register
router.get("/register", function(request, response) {
    response.render("register");
});

router.post("/register", function(request, response){
   var newUser = new User({username : request.body.username});
   User.register(newUser, request.body.password, function(err, user) {
       if (err) {
           console.log("Error creating user. " + err);
           request.flash("error", err.message);
           return response.redirect("/register");
       } else {
           passport.authenticate("local")(request, response, function() {
               request.flash("success", "Successfully registered");
               response.redirect("/");
           })
       }
   });
});

//login
router.get("/login", function(request, response) {
    response.render("login");
});

router.post("/login", passport.authenticate("local", {
    successRedirect : "/",
    failureRedirect : "/login"
}), function(request, response) {

});

router.get("/logout", function(request, response) {
    request.logout();
    request.flash("success", "Logged you out");
    response.redirect("/");
});

module.exports = router;